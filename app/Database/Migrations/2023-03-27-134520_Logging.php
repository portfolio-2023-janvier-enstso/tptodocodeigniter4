<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Logging extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'    =>[
                'type'  => 'BIGINT',
                'constraint'    =>11,
                'unsigned'  =>true,
                'auto_increment'=>true,
            ],
            'action'  =>[
                'type'  => 'VARCHAR',
                'constraint' => '200',
                'null'  => false,
            ],

            'date'  =>[
                'type'  =>  'Timestamp',
            ],
           
        ]);
        $this->forge->addKey('id',true);
        $this->forge->createTable('logging');
    }

    public function down()
    {
        $this->forge->dropTable('logging');
    }
}
