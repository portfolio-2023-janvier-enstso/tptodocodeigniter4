<?php

namespace App\Models;

use CodeIgniter\Model;

class TaskModel extends Model
{
    protected $DBGroup          = 'default';
    // la table mySql lié au model
    protected $table            = 'task';
    //permet à codeigniter de connaitre l'attribut id pour ainsi
    // générer automatiquement ses requêtes insert, update et delete
    protected $primaryKey       = 'id';
    //c'est une clé autoincrémenté ci4 doit le savoir 
    protected $useAutoIncrement = true;
    //point de départ de l'autoincrémenté (me semble t'il)
    protected $insertID         = 0;
    //permet de manipuler des objets au lieu de tableau 
    protected $returnType       = 'object';
    //Les delete sont gérés par des suppressions logiques et physique.
    //Utile dans certaines situations ou l'on doit conserver même les donnnées supprimées
    protected $useSoftDeletes   = false;
    //lié à allowedFiekds
    protected $protectFields    = true;
    //indispensable sans préciser quels sont les champs autorisés vous ne
    //Pourrez pas mettre à jour la bdd 
    // id qui est autoincrémenté n'est pâs concerné car géré directement
    //par la base de données et non votre programme.
    protected $allowedFields    = ['text','done','order'];

    // Dates
    //pas utilisées ici mais permet de générer et de générer automatiquement
    //Pour chaque occurence de la bdd une date de création et de modification
    protected $useTimestamps = false;
    //Validation 
    //Important permet de définir des règles simples de validation des
    //données pour la saisie utilisateur : voir la liste des 
    //clés : https://codeigniter.com/user-guide/libraries/validation.html
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [
        'text'   => 'required|string|max_length[100]',
        'order' => 'required|numeric',

    ];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    //Les callbacks : pas utilisées ici mais intéressent ce sont des points
    //d'entrée pour placer des méthodes qui seront automatiquement appelées
    //lorsque l'événement sur le modèle se produit!;
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
}
