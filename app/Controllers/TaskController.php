<?php

namespace App\Controllers;

use App\Controllers\BaseController;
// On hérite de BaseController
//N'oubliez pas d'ajouter la référence au namespace sinon la classe TaskModel
//ne sera pas utilisable dans ce source
use App\Models\TaskModel;
use App\Entities\Task;
use App\Models\LoggingModel;
use App\Entities\Logging;

class TaskController extends BaseController
{
    public function __construct()
    {
        //On place dans le constructeur toutes les classes nécessaires lors de l'appel
        //des différentes méthode Attention au double undrscore de Construct()
        //Les helpers sont des biliothèque de classes et de fonctions
        //que l'on va utiliser lors du développement. On les chages dans le constructeurs
        $this->helpers = ['form','url'];
        //On rajoute le modele en tant que données membre du Controleur ainsi on pourra y
        $this->taskModel=new TaskModel();
        $this->loggingModel= new LoggingModel();
        $this->loggingEntity = new Logging();
        
    }
    public function index()
    {
        //on instancie un nouveau Model
        $taskModel = new TaskModel();
        $loggingEntity = new Logging();
        $loggingModel= new LoggingModel();
        //Le modèle contient déjà toutes les métodes d'accès aux donées
        //Chaque méthode d'accès au données à le nom d'une primitive SQL:
        //where, whereIn, InnerJoin, on, orderBy etx.
        //la méthode findAll exécute la requête et renvoie le résultat.
        $tasks = $this->taskModel->orderBy('order')->findAll();
        //On rassemble toutes les données utilisées par la vue dans un tableau $data
        $data['tasks']=$tasks;
        $data['titre']="au boulot";
       
        // Chacun des éléments du tableau $data sera accessible dans la vue
        // view est une méthode hérite de baseControleur
        //on génère la vue
        return view('Task-index.php',$data);

    }

   
    //Il va falloir prévoir un nouveau formulaire permettant de créer une nouvelle tâche 
    public function create(){
        
        $data['titre']='Nouvelle Tâche';
        $insert = $this->loggingEntity->insertLog();
        $this->loggingModel->save($insert);
        return view ('Task-Form.php',$data);
    }
    //La méthode prévoit de reçevoir l'id de la tâche à supprimer
    public function delete(int $id){
        $this->taskModel->where(['id'=>$id])->delete();
        $insert = $this->loggingEntity->deleteLog();
        $this->loggingModel->save($insert);
        return redirect()->to('/')->with('message','Tâche supprimée');
    }
    // la méthode prévoit de recevoir l'id mais par défaut elle ne recevra aucun paramètre
    //c'est le seul moyen de créer une surchage en php.
    public function save (int $id=null){
        //Définir les règles de validation du formulaire 
        //Que l'on récupère de TaskModel
        $rules = $this->taskModel->getValidationRules();
        //On vérifie que l'on passe la validation
        if (!$this->validate($rules)){
            //En cas d'ereur on redirige vers la page prédécente
            return  redirect()->back()->withInput()->with('errors',$this->validator->getErrors());
        }
        else{
            //en cas de succès de la validation
            //On récupère les données du formulaire
            $form_data = [
                'order'=>$this->request->getPost('order'),
                'text'=>$this->request->getPost('text')
                
            ];
            if(!is_null($id)){
                $form_data['id']=$id;
            }
            //Créer une instance de notre tâche
            $task= new Task($form_data);
            // Génère insert/update
            $this->taskModel->save($task);
            return redirect()->to('/')->with('message',"Tâche sauvegardée");

        }

    
    }
    public function edit(int $id){
        $data['titre']="Modifier tâche";
        //On récupère la tâche à modifier
        $data['task']= $this->taskModel->find($id);
        $insert = $this->loggingEntity->updateLog();
        $this->loggingModel->save($insert);
        //On appelle la vue
        return view('Task-Form.php',$data);
    }
    public function done(int $id){
        $this->taskModel->update($id,['done'=>'1']);
        $insert = $this->loggingEntity->doneLog();
        $this->loggingModel->save($insert);
        return redirect()->to('/')->with('message','Tâche faite');
    }

    public function indexReorder(){
        $tasks= $this->taskModel->orderBy('order')->findAll();
        $index=10;
        //On renumérote l'ordre de toutes les tâches.
        foreach($tasks as $task){
            $task->order=$index;
            $index+=10;
        }
        $data['tasks']=$tasks;
        $data['titre']="Réordonner les tâches";
        return view('Task-indexReorder.php',$data);
    }

    public function SaveReorder(){
        $validation = \Config\Services::validation();
        $validation->setRule('order.*','ordre','required|numeric');
        if (!$validation->withRequest($this->request)->run()) {
            return redirect()->back()->withInput()->with('errors',$validation->getErrors());
        }
        else{
            $orders = $this->request->getPost('order[]');
            $ids = $this->request->getPost('id[]');
            $index = 0;
            foreach($ids as $id){
                $form_data = [
                    'order' => $orders[$index],
                    'id'    => $ids[$index],
                ];
                $task = new Task($form_data);
                $this->taskModel->save($task);
                $index++;
            }
            return redirect()->to('/')->with('message', "Tâches réorganisées");
        }
    }



}


