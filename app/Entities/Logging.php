<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class Logging extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];

    public function insertLog(){
        $action = "Ajout";
        date_default_timezone_set("Europe/Paris");
        $d=strtotime('now');
        $date = date('Y-m-d H:i:s',$d);
       $logging = array (
        'action' => $action,
        'date' => $date,
       );
    
       return $logging;
       
    }

    public function updateLog(){
        $action = "Modifer";
        date_default_timezone_set("Europe/Paris");
        $d=strtotime('now');
        $date = date('Y-m-d H:i:s',$d);
       $logging = array (
        'action' => $action,
        'date' => $date,
       );
    
       return $logging;
       
    }

    public function deleteLog(){
        $action = "supprimer";
        date_default_timezone_set("Europe/Paris");
        $d=strtotime('now');
        $date = date('Y-m-d H:i:s',$d);
       $logging = array (
        'action' => $action,
        'date' => $date,
       );
    
       return $logging;
       
    }

    public function doneLog(){
        $action = "done";
        date_default_timezone_set("Europe/Paris");
        $d=strtotime('now');
        $date = date('Y-m-d H:i:s',$d);
       $logging = array (
        'action' => $action,
        'date' => $date,
       );
    
       return $logging;
       
    }
}
